﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(float hh, float ww)
        {
            float m_hh = hh / 100;
            float bmi = ww / (m_hh * m_hh);
            string level = "";
                    if (bmi < 18.5)
                         {
                level = "太瘦";
                           }
                       else if (bmi > 18.5 && bmi < 24)
                          {
               level = "適中";
                           }
                       else if (bmi > 35)
             {
               level = "太胖";
            }
          
                         ViewBag.BMI = bmi;
            ViewBag.level = level;
            return View();

        }
    }

}